(ns a-game-of-thrones.core
  (:refer-clojure :exclude [==])
  (:use clojure.core.logic))
 
(defrel parent p c)
(defn child [c p] (parent p c))
(defrel male p)
(defrel female p)
 
(defn ancestor [x z]
  (conde
    [(parent x z)]
    [(fresh [y]
       (parent x y)
       (ancestor y z))]))
(defn descendant [x z]
  (conde
    [(child z x)]
    [(fresh [y]
       (child y x)
       (descendant y z))]))
 
(defn father [f c]
  (all
    (parent f c)
    (male f)))
(defn mother [m c]
  (all
    (parent m c)
    (female m)))
(defn son [s p]
  (all
    (child s p)
    (male s)))
(defn daughter [d p]
  (all
    (child d p)
    (female d)))
 
(defn grandfather [gp c]
  (fresh [p]
    (father gp p)
    (parent p c)))
	
(defn grandmother [gp c]
  (fresh [p]
    (mother gp p)
    (parent p c)))
	
(defn grandson [gs gp]
  (fresh [p]
    (child p gp)
    (son gs p)))
	
(defn granddaughter [gd gp]
  (fresh [p]
    (child p gp)
    (daughter gd gp)))
 
(defn sibling [x y]
  (fresh [p]
    (parent p x)
    (parent p y)
    (!= x y)))
	
(defn brother [b x]
  (all
    (sibling b x)
    (male b)))
	
(defn sister [s x]
  (all
    (sibling s x)
    (female s)))
 
(defn uncle [u n]
  (fresh [p]
    (brother u p)
    (parent p n)))
	
(defn aunt [a n]
  (fresh [p]
    (sister a p)
    (parent p n)))
	
(defn nephew [n s]
  (fresh [p]
    (sibling s p)
    (child n p)
    (male n)))
	
(defn niece [n s]
  (fresh [p]
    (sibling s p)
    (child n p)
    (female n)))
	
(defn cousin [c x]
  (fresh [cp xp]
    (parent cp c)
    (parent xp x)
    (sibling cp xp)))

(defn cousinM [c x]
  (fresh [cp xp]
    (parent cp c)
    (parent xp x)
    (sibling cp xp)
	(male c)))	
	
 
(defn -main
  [& args]
  (let [tree-file (slurp "resources/family-trees.txt")
        matches   (re-seq #"([A-Z][A-Z]) = ([a-zA-Z]+ [a-zA-Z]+) \(([MF])\)"
                          tree-file)
        people    (zipmap (map #(nth % 1) matches)
                          (map #(nth % 2) matches))
        relations (->> (re-seq #"([A-Z][A-Z])->([A-Z][A-Z])" tree-file)
                       (map #(map people %)))]
    (doseq [[_ p c] relations] (fact parent p c))
    (doseq [[_ _ p g] matches] (fact ({"M" male "F" female} g) p))
    (do
	      (println "The parents of Arya Stark:\n"
               (distinct (run* [q] (parent q "Arya Stark"))))
      (println "The complete ancestry of Asha Greyjoy:\n"
               (distinct (run* [q] (ancestor q "Asha Greyjoy"))))
      (println "The complete ancestry of Daenerys Targaryen:\n"
               (distinct (run* [q] (ancestor q "Daenerys Targaryen"))))
      (println "The children of Jaehaerys Targaryen:\n"
               (distinct (run* [q] (child q "Jaehaerys Targaryen"))))
      (println "The complete descendants of Jaehaerys Targaryen:\n"
               (distinct (run* [q] (descendant "Jaehaerys Targaryen" q)))) 
      (println "The siblings of Arya Stark:\n"
               (distinct (run* [q] (sibling "Arya Stark" q))))
      (println "The father of Tyrion Lannister:\n"
               (distinct (run* [q] (father q "Tyrion Lannister"))))
      (println "The children whose mother is Catelyn Tully:\n"
               (distinct (run* [q] (mother "Catelyn Tully" q))))
      (println "The people whose sister is Cersei Lannister:\n"
               (distinct (run* [q] (sister "Cersei Lannister" q))))
      (println "The uncles of Joffrey Baratheon:\n"
               (distinct (run* [q] (uncle q "Joffrey Baratheon"))))
      (println "The aunt of Robb Stark:\n"
               (distinct (run* [q] (aunt q "Robb Stark"))))
      (println "The cousin of Robb Stark:\n"
               (distinct (run* [q] (cousin q "Robb Stark"))))
      (println "The nephew of Catelyn Tully:\n"
               (distinct (run* [q] (nephew q "Catelyn Tully"))))
      (println " cousin :\n"
               (distinct (run* [q] (cousinM q "Lancel Lannister"))))
      )))
