AA = Rickard Stark (M)        AB = Eddard Stark (M)         AC = Catelyn Tully (F)
AD = Brandon Stark (M)        AE = Benjen Stark (M)         AF = Jon Snow (M)
AG = Robb Stark (M)           AH = Sansa Stark (F)          AI = Arya Stark (F)
AJ = Bran Stark (M)           AK = Rickon Stark (M)         AL = Hoster Tully (M)
AM = Minisa Whent (F)         AN = Edmure Tully (M)         AO = Lysa Tully (F)
AP = Jon Arryn (M)            AQ = Robert Arryn (M)         AR = Tytos Lannister (M)
AS = Tywin Lannister (M)      AT = Joanna Lannister (F)     AU = Kevan Lannister (M)
AV = Cersei Lannister (F)     AW = Jamie Lannister (M)      AX = Tyrion Lannister (M)
AY = Robert Baratheon (M)     AZ = Joffrey Baratheon (M)    BA = Myrcella Baratheon (F)
BB = Tommen Baratheon (M)     BC = Lancel Lannister (M)     BD = Steffon Baratheon (M)
BE = Stannis Baratheon (M)    BF = Selyse Florent (F)       BG = Shireen Baratheon (F)
BH = Renly Baratheon (M)      BI = Jaehaerys Targaryen (M)  BJ = Aerys Targaryen (M)
BK = Rhaella Targaryen (F)    BL = Rhaegar Targaryen (M)    BM = Elia Martell (F)
BN = Rhaenys Targaryen (F)    BO = Aegon Targaryen (M)      BP = Viserys Targaryen (M)
BQ = Daenerys Targaryen (F)   BR = Quellon Greyjoy (M)      BS = Balon Greyjoy (M)
BT = Euron Greyjoy (M)        BU = Victarion Greyjoy (M)    BV = Urrigon Greyjoy (M)
BW = Aeron Greyjoy (M)        BX = Rodrik Greyjoy (M)       BY = Maron Greyjoy (M)
BZ = Asha Greyjoy (F)         CA = Theon Greyjoy (M)        CB = Alannys Harlaw (F)
---------------------------------------------------------------------------------------
AA->AB, AA->AD, AA->AE, AB->AF, AB->AG, AB->AH, AB->AI, AB->AJ, AB->AK, AC->AG,
AC->AH, AC->AI, AC->AJ, AC->AK, AL->AC, AL->AN, AL->AO, AM->AC, AM->AN, AM->AO,
AO->AQ, AP->AQ, AR->AS, AR->AU, AS->AV, AS->AW, AS->AX, AT->AV, AT->AW, AT->AX,
AU->BC, AV->AZ, AV->BA, AV->BB, AY->AZ, AY->BA, AY->BB, BD->AY, BD->BE, BD->BH,
BE->BG, BF->BG, BI->BJ, BI->BK, BJ->BL, BJ->BP, BJ->BQ, BK->BL, BK->BP, BK->BQ,
BL->BN, BL->BO, BM->BN, BM->BO, BR->BS, BR->BT, BR->BU, BR->BV, BR->BW, BS->BX,
BS->BY, BS->BZ, BS->CA, CB->BX, CB->BY, CB->BZ, CB->CA