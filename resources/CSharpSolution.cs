void Main()
{
    Dictionary<string, Person> peopleMap =   new Dictionary<string, Person>();
	Dictionary<string, Person> initMap =   new Dictionary<string, Person>();
	Dictionary<Person, List<Person>> parentMap =   new Dictionary<Person, List<Person>>();
	Dictionary<Person, List<Person>> kidMap =   new Dictionary<Person, List<Person>>();

    string regexPattern = @"\w{2}\s=\s\w+\s\w+\s\([MF]\)";   			
	
	List<string> data=new List<string>();
	StringBuilder sb = new StringBuilder();
	
	using(StreamReader reader = new StreamReader(@"C:\PowerChanger\Workspaces\GUI\SimpleUi\src\data2.txt")){
    string line;    

    while((line = reader.ReadLine()) != null)
    {        
		data.Add(line);
    }
    reader.Close();
    }
	string peopleRawData=""; 
	string relationshipsRawData="";
	
	foreach(var line in data){
	   if(line.Contains("----")){
	      peopleRawData=sb.ToString();
		  sb = new StringBuilder();
	   }else{
	     sb.AppendLine(line);
	   }
	}
	relationshipsRawData=sb.ToString();
	
	//Console.Write("PEOPLE: "+peopleRawData);
	
	//Console.Write("Relations: "+relationshipsRawData);
	
	foreach (var item in Regex.Matches(peopleRawData, regexPattern)){
                string entry = item.ToString();
                string id = entry.Substring(0, 2);
                string name = entry.Substring(5, entry.Length - 9);
                string gender = entry.Substring(entry.Length - 2, 1);
                			
				Person p=new Person(id, name, (gender == "M"));
				peopleMap.Add(name,p);
				initMap.Add(id,p);
	}
			
    foreach (var item in relationshipsRawData.Split(new char[] { ',', ' ', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
            {
                string parent=item.Substring(0, 2);
				string child=item.Substring(4, 2);
				
			
				
				appendNested(parentMap,child,parent);
				appendNested(kidMap,parent,child);
																				
            }
			
			var r=dec("Jaehaerys Targaryen");
			
			r.Dump();
			
}

List<Person> dec(Person p,Dictionary<Person, List<Person>> parentMap){
   List<Person> ebeneN=parentMap[p];
   return ebeneN.AddAll(ebeneN.SelectMany(p,parentMap));
 }


void appendNested(Dictionary<Person, List<Person>> amap, string child, string parent ){
		if(amap.ContainsKey(parent)){
			    List<Person> vorhanden=amap[parent];
				vorhanden.Add(child);
				amap.Add(parent,vorhanden);
			}else{
			    amap.Add(parent,new List<Person>(child));
			}
}

 class Person{
	  public string Initialien { get; set; }
	  public string Name { get; set; }
	  public bool male { get; set; }
	  
	  public Person(string id,string name, bool male){
	   Initialien=id;
	   Name=name;
	   this.male=male;
	  }
	}