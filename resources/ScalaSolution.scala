object GotFamily {

  import scala.io._

    case class Person(initials: String, name: String, isMale: Boolean)

    var peopleMap: Map[String, Person] = Map.empty
    var initMap:   Map[String, Person] = Map.empty
    var parentMap: Map[Person, List[Person]] = Map.empty
    var kidMap:    Map[Person, List[Person]] = Map.empty

    def addToBag(map: Map[Person, List[Person]], key: String, value: String): Map[Person, List[Person]] ={
      map.getOrElse(initMap(key), null) match {
            case null => map.updated(initMap(key), getList(value))
            case ls =>   map.updated(initMap(key), getList(value) ::: ls)
        }
  } 
    
    def addPerson(p: Person): Unit =  {
            peopleMap += (p.name -> p)
            initMap += (p.initials -> p)
            //println(p.initials)
    }

    def readPeople(s: String): Unit = {
        s.trim.split("\\s+", 6).toList match {
            case List(i, eq, f, l, g, rest) => addPerson(Person(i, f + " " + l, g == "(M)"))
                                               readPeople(rest)
            case List(i, eq, f, l, g) =>       addPerson(Person(i, f + " " + l, g == "(M)"))
            case _ => Nil
        }
    }
    
    def getList(value:String):List[Person] = {
     val a=initMap.get(value)
     val b:Option[List[Person]]=a.map(x => List(x))
     
     if(b.isDefined)
       b.get
     else
       Nil    
    }

    def addBoth(a: String, b: String): Unit = { parentMap = addToBag(parentMap, a, b); kidMap = addToBag(kidMap, b, a) }

    def readPairs(s: String): Unit = s.split(", ").toList.filter(_.length() > 0).
            map(_.split("->") toList) foreach { case p @ List(a, b) => addBoth(a, b) }

    def readInput(input: Iterator[String]): Unit = input.foreach { line =>
        if (line.indexOf("----") > -1) {
            input.foreach (readPairs(_))
        }
        else readPeople(line)
    }

    def printSet(ps: List[Person]) = println(ps.map(_.name).toSet.mkString(", "))

    def descendants(p: Person): List[Person] = {
        val deses = parentMap.getOrElse(p, Nil);
        deses ::: deses.flatMap (descendants(_))
    }
    
    def ancestors  (p: Person): List[Person] = {
        val myParents = kidMap.getOrElse(p, Nil);
        myParents ::: myParents.flatMap (ancestors(_))
    }
    
    def siblings(p: Person): Set[Person] = ((kidMap.getOrElse(p, Nil) flatMap (parentMap(_))).toSet diff  Set(p))
    def aunts   (p: Person): List[Person] = parentMap.getOrElse(p, Nil) flatMap (sisters(_))
    def uncles  (p: Person): List[Person] = parentMap.getOrElse(p, Nil) flatMap (brothers(_))
    def brothers(p: Person): Set[Person] = siblings(p) filter (_.isMale)
    def sisters (p: Person): Set[Person] = siblings(p) filter (!_.isMale)
    
    def _1stcousinS(p:Person) = kidMap(p).flatMap(x => siblings(x)).flatMap(y => descendants(y) ) filter (_.isMale)
    
    def main(args: Array[String]) = {
        readInput(Source.fromFile("C:\\PowerChanger\\Workspaces\\GUI\\SimpleUi\\src\\data2.txt").getLines())
        
        
        println(_1stcousinS(peopleMap("Tobias Kaiser")))
        
//        println(relationShip(peopleMap("Arya Stark"),peopleMap("Eddard Stark")))
//        
//        printSet(ancestors(peopleMap("Daenerys Targaryen")))
//        printSet(descendants(peopleMap("Jaehaerys Targaryen")))
    }
    
}
